<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

$bannedUsers = array("vote","signup","cacti","video");

function ip_in_range($ip, $range) {
	if (strpos($range, '/') == false) {
		$range .= '/32';
	}
	// $range is in IP/CIDR format eg 127.0.0.1/24
	list($range, $netmask) = explode('/', $range, 2);
	$ip_decimal = ip2long($ip);
	$range_decimal = ip2long($range);
	$wildcard_decimal = pow(2, (32 - $netmask)) - 1;
	$netmask_decimal = ~ $wildcard_decimal;
	return (($ip_decimal & $netmask_decimal) == ($range_decimal & $netmask_decimal));
	}

$ipAddr = $_SERVER['REMOTE_ADDR'];

//var_dump($ipAddr);

if (!ip_in_range($ipAddr, "137.44.10.128/25")){
	$outsider = true;
	//exit("nah mate");
}

$ssoid = $_COOKIE["sucs_sso_id_v1"];

$sso_error = false;
$not_logged_in = true;

$curlsso = curl_init();
curl_setopt($curlsso, CURLOPT_URL, "https://sso.sucs.org/api/v1/?id=${ssoid}");
curl_setopt($curlsso,CURLOPT_RETURNTRANSFER,TRUE);
$sso_result = json_decode(curl_exec($curlsso));
curl_close($curlsso);

if (!$sso_result->apistate === "ok" || $sso_result->sucs_username === "") {
	$sso_error = true;
}

if ($sso_result->sucs_username !== null && $sso_result->sucs_username !== "") {
	$not_logged_in = false;
	$username = $sso_result->sucs_username;
}

if ($not_logged_in === false && in_array($sso_result->sucs_username,$bannedUsers) ) {
	$banned = true;
	//die("u r b&");
}

if ($_POST["unlock"] === "Unlock!" && !$outsider && !$banned && !$not_logged_in) {
	include_once("../doorkey.php");
	//curl door
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, "http://door.sucs.org/sendstatus.py?text=${username}%20via%20de%20web&timeout=1&fg=0,0,0&bg=128,255,128&unlock=1&key=${doorkey}");
	curl_setopt($curl,CURLOPT_RETURNTRANSFER,TRUE);
	curl_exec($curl);
	curl_close($curl);
	$success = true;
	//exit("door unlocked! (probably)");
}

?>


<!DOCTYPE html>
<html lang="en">
	<head>
		<title>SUCS doorkey</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="https://common.sucs.org/css/sucs.css">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</head>

	<body>

		<nav class="navbar navbar-light bg-light">
			<a class="navbar-brand" href="https://sucs.org"><img src="https://common.sucs.org/img/sucs_logo_white_trans.svg" /></a>
		</nav>

		<div class="h-100 container justify-content-center text-center">
			<div class="h-100 row d-flex justify-content-center align-items-center">
				<div class="col-8">
					<div class="card mb-4 mt-4">
						<?php
							if ($outsider || $sso_error) {
								print('
							<div class="card-body">
								<h4 class="card-title">❌</h4>
								<p>You must be connected to GuestNET to use this service.</p>
							</div>
								');
							} elseif ($banned) {
								print('
							<div class="card-body">
								<h4 class="card-title">🔨</h4>
								<p>You are banned from the room.</p>
							</div>
								');
							} elseif ($success) {
								print('
							<div class="card-body">
								<h4 class="card-title">✔️</h4>
								<p>Door unlocked!</p>
							</div>
								');
							} elseif ($not_logged_in) {
								print('
							<div class="card-body">
								<h4 class="card-title">🔑</h4>
								<p>Hi stranger click the button below to login</p>
								<a href=https://sso.sucs.org/?callbackapp=doorkey&callbackpath=/ class="btn btn-primary">Login!</a>
							</div>
								');
							} else {
								print('
							<div class="card-body">
								<h4 class="card-title">🔑🚪</h4>
								<p>Hi '.${username}.' click the button below to unlock the door!</p>
								<form method="post">
									<div class="form-group">
										<input type="submit" name="unlock" class="btn btn-primary" value="Unlock!" />
									</div>
								</form>
							</div>
								');
							}
						?>
					</div>
				</div>
			</div>
		</div>

	</body>

</html>
